<?php
require_once('DateValidator.php');

use PHPUnit\Framework\TestCase;

class DateValidatorTest extends TestCase
{

    public function test_valid_date_formate()
    {
        $timestamp = mt_rand(1, 2147385600);
        $dateString = date("d/m/Y",$timestamp);
        $result = DateValidator::validateHistoricalDate($dateString);
        $this->assertSame(true, $result->isValid());
    }

    public function test_invalid_date_formate()
    {
        $timestamp = mt_rand(1, 2147385600);
        $dateString = date("Y/m/d",$timestamp);
        $result = DateValidator::validateHistoricalDate($dateString);
        $this->assertSame(false, $result->isValid());

        $dateString = date("d-m-Y",$timestamp);
        $result = DateValidator::validateHistoricalDate($dateString);
        $this->assertSame(false, $result->isValid());

        $dateString = date("o3/12/1999",$timestamp);
        $result = DateValidator::validateHistoricalDate($dateString);
        $this->assertSame(false, $result->isValid());
    }

    public function test_date_is_historical()
    {
        $timestampNow = new Datetime("now");
        $timestamp = mt_rand(1, $timestampNow->format('U'));
        $dateString = date("d/m/Y",$timestamp);
        $result = DateValidator::validateHistoricalDate($dateString);
        $result->isValid();
        $this->assertSame(true, $result->historicalDate);
    }

    public function test_date_is_not_historical()
    {
        $timestampNow = new Datetime("now");
        $timestamp = mt_rand($timestampNow->format('U'), 2147385600);
        $dateString = date("d/m/Y",$timestamp);
        $result = DateValidator::validateHistoricalDate($dateString);
        $result->isValid();
        $this->assertSame(false, $result->historicalDate);
    }


}