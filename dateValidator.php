<?php
class DateValidator{

	public $dateString;
	private $message;
	public $historicalDate;

	static function validateHistoricalDate($dateString){
		$inst=new DateValidator();
		$inst->dateString=$dateString;
		return $inst;
	}

	public function isValid(){
		$dateExploded=explode("/", $this->dateString);
		if(count($dateExploded) != 3){
			return false;
		}
		[$day, $month, $year] = $dateExploded;

		//validate each part of the date is the correct length
		if(strlen($day)!=2 OR strlen($month)!=2 OR strlen($year)!=4){
			return false;		
		}

		//Checks the validity of the date
		if(!checkdate($month, $day, $year)){
			return false;
		}

		$date = $year . '-' . $month . '-' . $day;
		$this->historicalDate=$this->isHistorical($date);
		return true;
	}

	private function isHistorical($date){
		if (date("Y-m-d") > $date ) {
		    return true;
		}
		return false;
	}

	public function getMessage(){
		$format=$this->dateString;

		if (empty($this->dateString)){
			$message['text'] = 'Please enter a historic date';
			$message['type']='alert-danger';
			return $message;
		}

		if(!$this->isValid()){
			$message['text'] = 'The date is not valid. Please use DD/MM/YYYY format';
			$message['type']='alert-danger';
			return $message;
		}

		if($this->historicalDate){
			$message['type']='alert-success';
			$message['text'] = 'Valid historic date';
			return $message;
		}else{
			$message['text'] = 'Date is not a valid historic date';
			$message['type']='alert-danger';
			return $message;
		}

	}
}